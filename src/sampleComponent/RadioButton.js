import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const RadioButton = ({selected, onSelect, label, size}) => {
  return (
    <TouchableOpacity style={styles.radioButton} onPress={onSelect}>
      <View
        style={[
          styles.radioCircle,
          {
            width: size,
            height: size,
            borderColor: selected ? 'blue' : 'gray',
          },
        ]}>
        {selected && <View style={styles.selectedRb} />}
      </View>
      <Text style={styles.radioText}>{label}</Text>
    </TouchableOpacity>
  );
};

const App = () => {
  const [selected, setSelected] = React.useState('first');

  return (
    <View style={styles.container}>
      <RadioButton
        label="Pilihan 1"
        selected={selected === 'first'}
        onSelect={() => setSelected('first')}
        size={20} // Ubah ukuran radio button di sini
      />
      <RadioButton
        label="Pilihan 2"
        selected={selected === 'second'}
        onSelect={() => setSelected('second')}
        size={30} // Ubah ukuran radio button di sini
      />
      <RadioButton
        label="Pilihan 3"
        selected={selected === 'third'}
        onSelect={() => setSelected('third')}
        size={40} // Ubah ukuran radio button di sini
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  radioButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  radioCircle: {
    borderRadius: 50,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  selectedRb: {
    width: 10,
    height: 10,
    borderRadius: 50,
    backgroundColor: 'blue',
  },
  radioText: {
    fontSize: 16,
    color: 'black',
  },
});

export default App;
