import React, {useState} from 'react';
import {View, Modal, StyleSheet, TouchableOpacity} from 'react-native';
import {TextBold, TextMedium} from '../global';
import {Colors} from '../styles';
import Icon from 'react-native-vector-icons/AntDesign';

export const RadioButton = ({selected, onSelect, navigation}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.radioButton}
      onPress={onSelect}>
      <View style={styles.radioCircle}>{selected && navigation}</View>
    </TouchableOpacity>
  );
};

export const FilterTrackingModal = ({show, onClose}) => {
  const [selected, setSelected] = useState('first');
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.header}>
            <TextBold text="Filter" color={Colors.DARK_BLUE} size={16} />
            <TouchableOpacity onPress={onClose}>
              <Icon name="close" color={Colors.DARK_BLUE} size={16} />
            </TouchableOpacity>
          </View>
          <View style={{height: 2}} />
          <View>
            <View style={[styles.header, {borderBottomWidth: 0}]}>
              <TextMedium
                color={Colors.DARK_BLUE}
                size={16}
                text="Request Diskon"
              />
              <RadioButton
                selected={selected === 'first'}
                onSelect={() => setSelected('first')}
              />
            </View>
            <View style={[styles.header, {borderBottomWidth: 0}]}>
              <TextMedium
                color={Colors.DARK_BLUE}
                size={16}
                text="Request Subsidi"
              />
              <RadioButton
                selected={selected === 'second'}
                onSelect={() => setSelected('second')}
              />
            </View>
            <View style={[styles.header, {borderBottomWidth: 0}]}>
              <TextMedium
                color={Colors.DARK_BLUE}
                size={16}
                text="Request MRP"
              />
              <RadioButton
                selected={selected === 'third'}
                onSelect={() => setSelected('third')}
              />
            </View>
            <View style={{height: 33}} />
            <TouchableOpacity
              style={{
                height: 36,
                width: 328,
                borderRadius: 5,
                borderWidth: 2,
                borderColor: Colors.BLUE,
                backgroundColor: Colors.BLUE,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                marginBottom: 16,
              }}>
              <TextMedium color={Colors.WHITE} text="SIMPAN" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#E4E7EB',
    borderBottomWidth: 1,
  },
  body: {
    width: '100%',
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  radioCircle: {
    borderRadius: 16,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.BLUE,
    height: 16,
    width: 16,
  },
});
